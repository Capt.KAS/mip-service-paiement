import express from 'express';
import cors from "cors";
import morgan from "morgan";
import helmet from "helmet";
import compression from "compression";
import bodyParser from "body-parser";

import constants from './constants';

import { successMiddleware, errorMiddleware, notFoundMiddleware, displayMiddleware, Api, pad } from './lib';

import authRouter from './routes/Auth';
import payInRouter from './routes/PayIn';
import payOutRouter from './routes/PayOut';

const app = express();

app.use(bodyParser.json());
app.use(morgan("dev"));
app.use(helmet());
app.use(cors({
  origin: [constants.hostname],
  methods: ["GET", "POST", "PUT"]
}));
app.use(compression());

// app.use(displayMiddleware);
console.log(process.env.HEROKU_POSTGRESQL_BRONZE_URL);
app.get('/', (req, res) => {
  res.json({
    status: 'API is online'
  })
});

app.use('/auth', authRouter);
app.use('/payIn', payInRouter);
app.use('/payOut', payOutRouter);

app.use(successMiddleware);
app.use(errorMiddleware);
app.use(notFoundMiddleware);

app.listen(process.env.PORT || constants.port, function () {
  console.log('\x1b[33m');
  const tmp = pad(`🚀     Api running at http://${constants.hostname}:${process.env.PORT || constants.port}/     🚀`, 78);
  console.log('================================================================================');
  console.log('#                                                                              #');
  console.log('#                  Welcome to the MIP Payement Micro Service                   #');
  console.log(`#${tmp}#`);
  console.log('#                    Developed by antoine.casse@epitech.eu                     #');
  console.log('#                                                                              #');
  console.log('================================================================================');
  console.log('\x1b[0m')
  Api.init();
});

