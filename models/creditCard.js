const creditCardModel = {
  cardType: { type: "string", values: ["CB_VISA_MASTERCARD", "DINERS", "MASTERPASS", "AMEX" , "MAESTRO", "P24", "IDEAL", "BCMC", "PAYLIB"] },
  currency: { type: "string", format: /[A-Z]{3}/ },
  cardKey: { type: "string" },
};

export default creditCardModel;