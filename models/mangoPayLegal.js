import moment from 'moment';

import mangoPayAddressModel from "./mangoPayAddress";

const mangoPayLegal = {
  companyName: { name: 'Name' },
  companyNumber: { name: 'CompanyNumber' },
  companyAddress: { name: 'HeadquartersAddress' },
  companyEmail: { name: 'Email' },
  firstname: { name: 'LegalRepresentativeFirstName' },
  lastname: { name: 'LegalRepresentativeLastName' },
  email: { name: 'LegalRepresentativeEmail' },
  dob: { name: 'LegalRepresentativeBirthday', modifier: (val) => moment(val, "YYYY-MM-DD").utc().toDate().getTime() / 1000 },
  nationality: { name: 'LegalRepresentativeNationality' },
  country: { name: 'LegalRepresentativeCountryOfResidence' },
  ownerAddress: { type: 'object', model: mangoPayAddressModel, name: 'LegalRepresentativeAddress' },
};

export default mangoPayLegal;