const documentType = [
  "IDENTITY_PROOF", "REGISTRATION_PROOF", "ARTICLES_OF_ASSOCIATION", "SHAREHOLDER_DECLARATION", "ADDRESS_PROOF"
]

const kycModel = {
  documentType: { type: "string", values: documentType, required: true },
  pages: { type: "string", required: true },
};

export default kycModel;