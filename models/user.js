import userClientModel from "./userClient";
import userBusinessModel from "./userBusiness";
import creditCardModel from "./creditCard";
import bankAccountModel from "./bankAccount";
import kycModel from "./kyc";

const userModel = {
  userType: { type: "string", values: ["CLIENT", "BUSINESS"], required: true },
  userInfo: { type: "object", model: (type) => type === "CLIENT" ? userClientModel : userBusinessModel, required: true, needs: "userType" },
  creditCard: { type: "object", model: creditCardModel},
  bankAccount: { type: "object", model: bankAccountModel},
  legal: { type: "object", model: kycModel},
};

export default userModel;