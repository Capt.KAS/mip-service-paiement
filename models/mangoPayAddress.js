const mangoPayAddressModel = {
  line1: { name: 'AddressLine1' },
  line2: { name: 'AddressLine2' },
  city: { name: 'City' },
  region: { name: 'Region' },
  postalCode: { name: 'PostalCode' },
  country: { name: 'Country' },
};

export default mangoPayAddressModel;