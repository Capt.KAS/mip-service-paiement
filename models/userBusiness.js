import { countries } from'../values';
import addressModel from './address';
import { dobDateCheck } from '../lib/utils';

const userBusinessModel = {
  companyName: { type: "string", maxLength: 50, minLength: 2, required: true },
  companyNumber: { type: "string", maxLength: 50, minLength: 2, required: true },
  firstname: { type: "string", maxLength: 50, minLength: 2, required: true },
  lastname: { type: "string", maxLength: 50, minLength: 2, required: true },
  email: { type: "string", pattern: /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/, minLength: 6 },
  dob: { type: "string", pattern: /[0-9]{4}-[0-9]{2}-[0-9]{2}/, required: true, check: dobDateCheck },
  nationality: { type: "string", pattern: /[A-Z]{2}/, values: countries, required: true },
  country: { type: "string", pattern: /[A-Z]{2}/, values: countries, required: true },
  companyAddress: { type: "object", model: addressModel },
  companyEmail: { type: "string", maxLength: 100, minLength: 6, required: true },
  ownerAddress: { type: "object", model: addressModel },
};

export default userBusinessModel;