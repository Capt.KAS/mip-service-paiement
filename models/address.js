import { countries } from'../values';

const addressModel = {
  line1: { type: "string", maxLength: 100, required: true },
  line2: { type: "string", maxLength: 100 },
  city: { type: "string", maxLength: 50, minLength: 2, required: true },
  region: { type: "string", maxLength: 50, minLength: 2, required: true },
  postalCode: { type: "string", maxLength: 50, required: true },
  country: { type: "string", pattern: /[A-Z]{2}/, values: countries, required: true },
};

export default addressModel;