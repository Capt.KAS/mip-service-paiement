import moment from 'moment';

import mangoPayAddressModel from "./mangoPayAddress";

const mangoPayNatural = {
  firstname: { name: 'FirstName' },
  lastname: { name: 'LastName' },
  email: { name: 'Email' },
  dob: { name: 'Birthday', modifier: (val) => moment(val, "YYYY-MM-DD").utc().toDate().getTime() / 1000 },
  nationality: { name: 'Nationality' },
  country: { name: 'CountryOfResidence' },
  address: { type: 'object', model: mangoPayAddressModel, name: 'Address' },
  occupation: { name: 'Occupation' },
  incomeRange: { name: 'IncomeRange' },
};

export default mangoPayNatural;