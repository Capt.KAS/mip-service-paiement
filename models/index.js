export {default as addressModel} from './address';
export {default as bankAccountModel} from './bankAccount';
export {default as creditCardModel} from './creditCard';
export {default as kycModel} from './kyc';
export {default as userBusinessModel} from './userBusiness';
export {default as userClientModel} from './userClient';
export {default as userModel} from './user';
export {default as payOutModel} from './payOut';
export {default as payInModel} from './payIn';

export {default as mangoPayNaturalModel} from './mangoPayNatural';
export {default as mangoPayLegalModel} from './mangoPayLegal';
export {default as mangoPayBankAccountModel} from './mangoPayBankAccount';