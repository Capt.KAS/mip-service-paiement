import mangoPayAddressModel from "./mangoPayAddress";

const bankAccountModel = {
  ownerAddress: { name: 'OwnerAddress', type: 'object', model: mangoPayAddressModel },
  ownerName: { name: 'OwnerName' },
  iban: { name: 'IBAN' },
  bic: { name: 'BIC' },
};

export default bankAccountModel;