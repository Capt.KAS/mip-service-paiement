import addressModel from './address';

const bankAccount = {
  ownerName: { type: "string", required: true, minLength: 2, maxLength: 50 },
  ownerAddress: { type: "object", model: addressModel, required: true },
  iban: { type: "string", required: true, pattern: /^([A-Z]{2}[ \-]?[0-9]{2})(?=(?:[ \-]?[A-Z0-9]){9,30}$)((?:[ \-]?[A-Z0-9]{3,5}){2,7})([ \-]?[A-Z0-9]{1,3})?$/ },
  bic: { type: "string", pattern: /^[a-z]{6}[2-9a-z][0-9a-np-z]([a-z0-9]{3}|x{3})?$/i },
};

export default bankAccount;