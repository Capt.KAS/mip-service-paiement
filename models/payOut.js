const payOutModel = {
  userId: { type: "string", required: true },
  amount: { type: "int", positive: true, required: true },
  fee: { type: "int", positive: true, required: true },
  currency: { type: "string", required: true, pattern: /[A-Z]{3}/ },
  message: { type: "string" },
};

export default payOutModel;