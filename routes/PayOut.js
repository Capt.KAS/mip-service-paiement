import express from 'express';
import { payOutModel } from '../models';
import { Api, sql, parser } from '../lib';

const router = express.Router();

router.post('/', async (req, res, next) => {

  const { errors, result } = parser(req.body, payOutModel)
  if (errors.length) {
    res.locals.code = 400;
    res.locals.message = "Invalid request parameters";
    res.locals.data = errors;
    next("Error");
  }

  const { userId, amount, fee, message, currency } = result;

  // Get user from DB

  const dbRes = await sql.getUser(userId);
  if (dbRes === null) {
    res.locals = {
      code: 404,
      message: "User not found",
    }
    return next("Error");
  }

  const { mipid: mipId, pspid: pspId, walletid: walletId, bankid: bankId } = dbRes;

  if (bankId === null) {
    res.locals = {
      code: 403,
      message: "User does not have a bank account",
    }
    return next("Error");
  }

  const pspRes = await Api.payOut({ mipId, pspId, bankId, walletId, amount, currency, fee, message });
  if (pspRes === null) {
    res.locals = {
      code: 500,
      message: "PSP did not respond to pay out",
      data: pspRes,
    }
    return next("Error");
  } else if (pspRes.success === false) {
    res.locals = {
      code: 403,
      message: "Operation was not allowed by PSP",
      data: pspRes,
    }
    return next("Error");
  }

  res.locals = {
    code: "200",
    message: "Pay out registered",
    data: pspRes,
  }
  next();
});

export default router;