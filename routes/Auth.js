import express from 'express';

import { parser, sql, Api } from '../lib';
import { userModel, mangoPayNaturalModel, mangoPayLegalModel } from '../models/';

const router = express.Router();

const dataParser = (req, res, next) => {
  const { body } = req;

  const { errors, result } = parser(body, userModel, {noRequire: req.method === "PATCH" ? true : undefined});

  console.log("EDIT", result);

  if (errors.length) {
    res.locals.code = 400;
    res.locals.message = "Invalid request parameters";
    res.locals.data = {errors, user: null};
    next("Error");
  }
  req.parsedBody = result;
  next();
}

router.route('/:id')

  /******************/
  /*                */
  /*    GET USER    */
  /*                */
  /******************/

  .get(async (req, res, next) => {

    // Get from DB

    const dbRes = await sql.getUser(req.params.id);
    if (dbRes === null) {
      res.locals = {
        code: 404,
        message: "User not found",
      }
      return next("Error");
    }

    const { pspid: pspId, seller, cardtype: cardType, cardcurrency: cardCurrency, cardkey: cardKey, bankid: bankId } = dbRes;

    // Get user and bank details from PSP

    const apiRes = await Api.getUser({pspId, seller, bankId});
    if (apiRes === null) {
      res.locals = {
        code: 404,
        message: "User not found",
      }
      return next("Error");
    }

    const result = {
      userType: seller ? "BUSINESS" : "CLIENT",
      userInfo: apiRes.user,
      creditCard: {
        cardType,
        currency: cardCurrency,
        cardKey
      },
      bankAccount: apiRes.bank
    };

    // Get KYC details 
    
    result.legal = {
      /* TODO */
    };

    res.locals = {
      code: 200,
      message: "User found",
      data: result,
    }
    next();
  })

  /***********************/
  /*                     */
  /*    REGISTER USER    */
  /*                     */
  /***********************/

  .put(dataParser, async (req, res, next) => {
    const mipId = req.params.id;
    const seller = req.parsedBody.userType === "BUSINESS";

    // Check if not present
    
    const dbRes = await sql.getUser(req.params.id);
    if (dbRes !== null) {
      res.locals.code = 409;
      res.locals.message = "User already created";
      return next("Error");
    }

    // Create User

    const createUserRes = req.parsedBody.userType === "CLIENT" ? 
      await Api.createNaturalUser(req.parsedBody.userInfo) : await Api.createLegalUser(req.parsedBody.userInfo);
    if (createUserRes === null) {
      res.locals.code = 500;
      res.locals.message = "PSP Could not register user";
      return next("Error");
    }

    const pspId = createUserRes.Id;

    // Create Wallet

    const createWalletRes = await Api.createWallet({pspId, mipId});
    if (createWalletRes === null) {
      res.locals.code = 500;
      res.locals.message = "PSP Could not create wallet";
      return next("Error");
    }

    const walletId = createWalletRes.Id;
    let othErrors = []

    // Add credit card type

    let creditCardRes = null;
    if (req.parsedBody.creditCard && req.parsedBody.creditCard.cardType) {
      if (req.parsedBody.creditCard.currency === undefined) {
        othErrors.push({field: 'userInfo.creditCard', reason: 'No currency defined'});
      } else {
        creditCardRes = await Api.setCreditCardType({
          pspId,
          type: req.parsedBody.creditCard.cardType,
          currency: req.parsedBody.creditCard.currency
        });
        console.log("CC RES", creditCardRes)
        if (creditCardRes === null)
          othErrors.push({field: 'userInfo.creditCard', reason: 'Could not set credit card type'});
      }
    }

    // Add bankAccount

    let bankAccount = null;
    if (req.parsedBody.bankAccount) {
      const baRes = await Api.setBankAccount({pspId, bankAccount: req.parsedBody.bankAccount});
      if (baRes && baRes !== null) {
        bankAccount = baRes.Id;
      } else
        othErrors.push({field: 'userInfo.bankAccount', reason: 'Could not set bank account'});
    }

    // Add legal

    /* TODO */

    // Register in DB

    const creditcardType = creditCardRes ? creditCardRes.cardType : null;
    const creditcardId = creditCardRes ? creditCardRes.id : null;
    const creditcardCur = creditCardRes ? req.parsedBody.creditCard.currency : null;
    const dbInsert = await sql.addUser({
      mipId,
      pspId,
      seller,
      walletId, 
      cardId: creditcardId, 
      cardType: creditcardType, 
      cardCurrency: creditcardCur, 
      bankId: bankAccount
    });
    if (dbInsert === null) {
      res.locals.code = 500;
      res.locals.message = "Could not register user in database";
      return next("Error");
    }

    // Final

    res.locals.data = {};
    if (othErrors.length > 0) {
      res.locals.code = 206;
      res.locals.message = "User created but certain informations not set";
      res.locals.data.errors = othErrors;
    } else {
      res.locals.code = 201;
      res.locals.message = "User created";
      res.locals.data.errors = null;
    }

    const userInfo = parser(createUserRes, req.parsedBody.userType === "BUSINESS" ? 
    mangoPayLegalModel : mangoPayNaturalModel, {reverse: true}).result;

    res.locals.data.user = {
      userType: req.parsedBody.userType,
      userInfo,
      creditCard: {
        cardType: creditCardRes && creditCardRes.cardType || null,
        cardKey: null,
      },
      bankAccount: {
        ownerAddress: req.parsedBody.bankAccount && req.parsedBody.bankAccount.ownerAddress || null,
        ownerName: req.parsedBody.bankAccount && req.parsedBody.bankAccount.ownerName || null,
        iban: req.parsedBody.bankAccount && req.parsedBody.bankAccount.iban || null,
        bic: req.parsedBody.bankAccount && req.parsedBody.bankAccount.bic || null,
      },
      legal: { /* TODO */ },
    }

    /* Add card registration info if card given */

    if (creditCardRes !== null) {
      res.locals.data.user.creditCard = creditCardRes;
      res.locals.data.user.creditCard.cardKey = null;
    }

    next();
  })

  /*********************/
  /*                   */
  /*    UPDATE USER    */
  /*                   */
  /*********************/

  .patch(dataParser, async (req, res, next) => {

    // Get from DB

    console.log('PUT USER', req.parsedBody);

    const dbRes = await sql.getUser(req.params.id);
    if (dbRes === null) {
      res.locals = {
        code: 404,
        message: "User not found",
      }
      return next("Error");
    }

    // Update in PSP

    if (req.parsedBody.userInfo) {
      req.parsedBody.userInfo.pspId = dbRes.pspid;
      const updateUserRes = dbRes.seller === false ? 
        await Api.updateNaturalUser(req.parsedBody.userInfo) : await Api.updateLegalUser(req.parsedBody.userInfo);
      if (updateUserRes === null) {
        res.locals.code = 500;
        res.locals.message = "PSP Could not modif user";
        return next("Error");
      }
    }

    let othErrors = [];

    // Update credit card type

    let creditCardRes = null;
    if (req.parsedBody.creditCard && req.parsedBody.creditCard.cardType) {
      if (req.parsedBody.creditCard.currency === undefined) {
        othErrors.push({field: 'userInfo.creditCard', reason: 'No currency defined'});
      } else {
        creditCardRes = await Api.setCreditCardType({
          pspId: dbRes.pspid,
          type: req.parsedBody.creditCard.cardType,
          currency: req.parsedBody.creditCard.currency,
          replaceId: dbRes.cardid
        });
        console.log("CC RES TYPE", creditCardRes)
        if (creditCardRes === null)
          othErrors.push({field: 'userInfo.creditCard', reason: 'Could not set credit card type'});
      }
    } else if (req.parsedBody.creditCard && req.parsedBody.creditCard.cardKey) {
      if (dbRes.cardkey) {
        othErrors.push({field: 'userInfo.creditCard', reason: 'Card key already set'});
      } else {
        creditCardRes = await Api.setCreditCardKey({pspId: dbRes.pspid, key: req.parsedBody.creditCard.cardKey, cardId: dbRes.cardid, replaceId: dbRes.cardkey ? dbRes.cardid : undefined});
        console.log("CC RES KEY", creditCardRes)
        if (creditCardRes === null)
          othErrors.push({field: 'userInfo.creditCard', reason: 'Could not set credit card key'});
      }
    }

    // Update bank account

    let bankAccount = undefined;
    if (req.parsedBody.bankAccount) {
      const baRes = dbRes.bankId !== null ?
        await Api.replaceBankAccount({pspId: dbRes.pspid, bankAccountId: dbRes.bankid, bankAccount: req.parsedBody.bankAccount}) :
        await Api.setBankAccount({pspId: dbRes.pspid, bankAccount: req.parsedBody.bankAccount});
      if (baRes && baRes !== null) {
        bankAccount = baRes.Id;
      } else
        othErrors.push({field: 'bankAccount', reason: 'Could not set bankAccount'});
    }

    // Update in DB

    if (bankAccount !== undefined || creditCardRes !== null) {
      let updateRes;
      if (req.parsedBody.creditCard && req.parsedBody.creditCard.cardType && creditCardRes) {
        updateRes = await sql.updateUserItems({
          mipId: req.params.id,
          cardId: creditCardRes.id,
          cardType: creditCardRes.cardType,
          cardCurrency: req.parsedBody.creditCard.currency,
          cardKey: req.parsedBody.creditCard && req.parsedBody.creditCard.cardKey || null,
          bankdId: bankAccount
        });
      } else if (req.parsedBody.creditCard && req.parsedBody.creditCard.cardKey && creditCardRes)
        updateRes = await sql.updateUserItems({
          cardId: creditCardRes.CardId,
          mipId: req.params.id,
          cardKey: req.parsedBody.creditCard.cardKey, 
          bankdId: bankAccount,
        });
      else
        updateRes = await sql.updateUserItems({
          mipId: req.params.id, bankId: bankAccount
        });
      if (updateRes === null) {
        res.locals = {
          code: 500,
          message: "User not updated",
        }
        return next();
      }
    }

    // Final

    res.locals.data = {}
    if (othErrors.length > 0) {
      res.locals.code = 206;
      res.locals.message = "User updated but certain informations not set";
      res.locals.data.errors = othErrors;
    } else {
      res.locals.code = 200;
      res.locals.message = "User updated";
      res.locals.data.errors = null;
    }

    // Set modified user in response

    res.locals.data.user = {
      userType: req.parsedBody.userType,
      legal: { /* TODO */ },
    };

    if (req.parsedBody.userInfo) {
      res.locals.data.user.userInfo = parser(createUserRes, req.parsedBody.userType === "BUSINESS" ? 
        mangoPayLegalModel : mangoPayNaturalModel, {reverse: true}).result;
    }

    if (req.parsedBody.creditCard) {
      res.locals.data.user.creditCard = {
        cardType: creditCardRes && creditCardRes.cardType || null,
        cardKey: null,
      };
    }

    if (req.parsedBody.bankAccount) {
      res.locals.data.user.bankAccount = {
        ownerAddress: req.parsedBody.bankAccount && req.parsedBody.bankAccount.ownerAddress || null,
        ownerName: req.parsedBody.bankAccount && req.parsedBody.bankAccount.ownerName || null,
        iban: req.parsedBody.bankAccount && req.parsedBody.bankAccount.iban || null,
        bic: req.parsedBody.bankAccount && req.parsedBody.bankAccount.bic || null,
      };
    }

    /* Add card registration info if card given */

    if (creditCardRes !== null) {
      res.locals.data.user.creditCard = creditCardRes;
      res.locals.data.user.creditCard.cardKey = req.parsedBody.creditCard && req.parsedBody.creditCard.cardKey || null;
    }

    next();
  })

  .delete((req, res, next) => {
    res.locals.code = 501;
    res.locals.message = "Not implemented";
    next();
  })

export default router;