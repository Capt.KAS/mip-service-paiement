import express from 'express';

import { parser, sql, Api } from '../lib';
import { payInModel } from '../models';

const router = express.Router();


router.post('/', async (req, res, next) => {
  const { errors, result } = parser(req.body, payInModel)
  if (errors.length) {
    res.locals.code = 400;
    res.locals.message = "Invalid request parameters";
    res.locals.data = errors;
    next("Error");
  }

  const { userId, amount, fee, currency } = result;

  const dbRes = await sql.getUser(userId);
  if (dbRes === null) {
    res.locals = {
      code: 404,
      message: "User not found",
    }
    return next("Error");
  }

  const { mipid: mipId, pspid: pspId, walletid: walletId, cardid: cardId, cardkey: cardKey, cardtype: cardType, cardcurrency: cardCurrency } = dbRes;

  if (cardId === null) {
    res.locals = {
      code: 403,
      message: "User does not have a pay in method registered",
    }
    return next("Error");
  }

  if (cardKey === null) {
    res.locals = {
      code: 403,
      message: "User did not complete card registration process",
    }
    return next("Error");
  }

  const chosenCur = currency || cardCurrency;
  const pspRes = await Api.payIn({ mipId, pspId, walletId, cardId, cardType, cardCurrency: chosenCur, amount, fee});
  if (pspRes === null) {
    res.locals = {
      code: 500,
      message: "PSP did not respond to pay in",
      data: pspRes,
    }
    return next("Error");
  } else if (pspRes.success === false) {
    res.locals = {
      code: 403,
      message: "Operation was not allowed by PSP",
      data: pspRes,
    }
    return next("Error");
  }

  res.locals = {
    code: "200",
    message: "Pay in registered",
    data: pspRes,
  }
  next();
});

export default router;