# MIP - Payement service documentation

This is the documentation for the MIP payement micro-service which acts as a separate API.

This API acts as an intermediary between the MIP server and MangoPay, the payement service chosen to be used.

The API is coded in Node JS and requires a mySQL server, node and npm installed beforehand aswell as, depending on the context, either pm2 or nodemon to run the app, respectively for server-side and local developement. (see *package.json*)

Developement and documentation by Antoine Casse - For all questions use antoine.casse@epitech.eu.

 ---

## Install

Here are the steps for installing the API wether it is on your local machine or on your server.

* After having cloned the project, the first step to install the API is to install all required dependencies using npm.

      $  npm install

* You must create a credentials.js file using the following template and place it at the root of the repository before trying to run the API.

      credentials = {};

      credentials.sql = {
        host: "HOST",
        user: "USER",
        password: "PASSWORD",
      }

      credentials.mangoPay = {
        clientId: "CLIENTID",
        apiKey: "APIKEY"
      }

      modules.export = credentials;

  With all capitalized values replaced by their real value, the database to be used is already set in *constants.js*.

* Then the databases must be loaded, using mysql client you must do

      $  mysql -u USER < mip_pay.sql

  This file will build the databases and tables needed.

  

* Once done, proceed with either of the following startup scripts

      $  npm run local

  Runs a local version of the API for developement, uses nodemon, mangoPay sandbox and mip_pay as database name.


      $  npm run test
    
  Runs a test version of the API for testing in real conditions, uses pm2, mangoPay sandbox and mip_pay_test as database.

      $  npm run prod
    
  Runs the production version of the API, uses pm2, mangoPay real data and mip_pay_prod as database.

Depending on the script, different information will be loaded into constants.js, wich in turn will be used all accross the app.

Once running, the processes launched using pm2 can be accessed using pm2 from the service using an ssh connection.

      $  pm2 list  // List all processes

      $  pm2 log [PROCESS]  // Access the log of [PROCESS] (either 'prod' or 'test')

      $  pm2 restart [PROCESS]  // Restart a process

---

## Usage

This part will present basic usage information and processes, for the complete api route documentation, see the swagger file in *mip_pay.yaml*. All communications are handled in JSON

* Basic registration

  The first step is to create an user using the PUT /auth/:id route (replace :id with the id used by the MIP back end), the important informations here are the ones contained in userType, the type either CLIENT or BUSINESS, and userInfo (card, bank account and kyc registration can be done now or later), this will create a mangoPay sub-user and create a wallet assigned to the new user.

* Edit profile

  From there, all user informations (besides its userType) can be modified using a PATCH /auth/:id route, the modifyable parameters are the same as when creating the user, with the exception of all fields being optional here.

* Add card information

  This step can be done either when you are creating a new user or modifying an existing one, there are two parts for the card registration, the first one includes giving cardType and currency to the creditCard object of a user (either creating or editing). The server will respond with a creditCard object containing two fields, **accessKey** and **preregistrationData** wich are needed to register the credit card using the mangoPay process (see mangoPay documentation), this in turn will return a token that you will give to the user using a PATCH, storing it in creditCard.cardKey, if the response to this last request is positive then the card is ready to be used.

* Pay IN

  The pay-in process defines the act of taking money from a user's registered credit card and placing it in his wallet, while taking a fee.  All money values are in the smallest possible division of the currency used (ergo 12,60€ is "1260" (ct), but 120Y is "120").

  There are three amounts to enter, **amount** will be the amount that the user receives in his wallet, **fee** will be the amount to be taken from the credit card and placed in the MIP wallet for fees, The total taken from the credit card will be equal to **amount** + **fee**. All values are in the currency provided when registering the card by default or in the **currency** provided.

* Pay OUT

  The pay-out process defines the act of taking money from a user's wallet and placing it in his registered bank account, while taking a fee. All money values are in the smallest possible division of the currency used (ergo 12,60€ is "1260" (ct), but 120Y is "120").

  There are three amounts to enter, **amount** will be the amount that the user receives in his bank account (before his bank's fees if applicable), **fee** will be the amount to be taken from the user's wallet and placed in the MIP wallet for fees, The total taken from the wallet will be equal to **amount** + **fee**. All values are in the **currency** provided.

---

## Architecture



---

## Database

The different databases all use the same single table called **users** composed of the following columns.

  - mipId - *VARCHAR(50) NOT NULL PRIMARY UNIQUE* - The id used by the MIP back-end.
  - pspId - *VARCHAR(50) NOT NULL UNIQUE* - The id used by the Payement Service Provider.
  - seller - *TINYINT(1) NOT NULL* - Wheter this user is a business or otherwise a client (true or false).
  - status - *VARCHAR(10) NOT NULL* - Either "CREATED", "FILLED" or "READY".
  - walletId - *VARCHAR(10)* - The id used by the psp to represent a user's wallet.
  - cardId - *VARCHAR(10)* - The id used by the psp to represent a user's credit card.
  - cardType - *VARCHAR(10)* - The type of credit card entered (example: CB_VISA_MASTERCARD).
  - cardCurrency - *VARCHAR(3)* - The currency registered with the card, ISO-3 format (example: EUR).
  - cardKey - *VARCHAR(50)* - The key used in the final step of the card registration process, if not set this means that user can not use his card yet.
  - bankId - *VARCHAR(10)* - The id used by the psp to represent a user's bank account informations.
  - created - *DATETIME DEFAULT NOW()* - Date of the registration.

For more information see the mip_pay.sql file.