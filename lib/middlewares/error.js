import { errLog } from '../../lib';

const errorMiddleware = (err, req, res, next) => {
  errLog("ERROR", err === "Error" ? res.locals : err);
  if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
    console.error(err);
    return res.status(400).json({
      code: 400,
      status: "KO",
      message: "JSON error in body"
    });
  }

  const code = res.locals.code || 500;
  res.status(code).json({
    code,
    status: "KO",
    message: code === 500 ? "Internal server error" : res.locals.message || "Internal server error",
    data: res.locals.data || null,
  });
};

export default errorMiddleware