const notFoundMiddleware = (req, res, next) => {
  if (res.locals.code !== undefined)
    return;
  res.status(404).json({
    code: 404,
    status: "KO",
    message: "Could not resolve path.",
    data: null,
  });
}

export default notFoundMiddleware;