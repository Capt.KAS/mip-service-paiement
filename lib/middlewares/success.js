const successMiddleware = (req, res, next) => {
  if (res.locals === undefined || res.locals.code === undefined)
    return next();
  res.status(res.locals.code).json({
    code: res.locals.code,
    status: res.locals.code >= 200 && res.locals.code <= 299 ? "OK" : "KO",
    message: res.locals.message || "",
    data: res.locals.data || null,
  });
}

export default successMiddleware