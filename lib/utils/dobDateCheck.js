import moment from 'moment';

const dobDateCheck = (val) => {
  const dob = moment(val, "YYYY-MM-DD");
  return dob.isValid() === true &&
  dob.isAfter(moment("1910-01-01")) &&
  dob.isBefore(moment().subtract(18, 'year'))
}

export default dobDateCheck;