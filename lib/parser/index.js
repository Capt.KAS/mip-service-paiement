

const parser = (body, model, options) => {
  const res = {
    errors: [],
    result: {}
  }
  if (options === undefined)
    options = {};

  const keys = Object.keys(model);
  let i = 0;
  while (i < keys.length) {
    const elem = keys[i];
    const attr = model[elem];
    let data = body[options && options.reverse ? attr.name : elem];
    i += 1;

    //Name

    const name = options && options.reverse ? elem : attr.name || elem;

    // Presence

    if (data === undefined || data === null) {
      if (attr.required === true && options.noRequire !== true)
        res.errors.push({ field: elem, reason: 'MISSING' });
      continue;
    }

    // Values

    if (attr.values !== undefined) {
      if (attr.values.includes(data) === false) {
        res.errors.push({ field: elem, reason: 'VALUE' });
        continue;
      }
    }

    if (attr.check !== undefined) {
      console.log(attr.check(data));
      if (attr.check(data) === false) {
        res.errors.push({ field: elem, reason: 'VALUE' });
        continue;
      }
    }

    // Type specific caracteristics

    if (attr.type === "string") {
      
      // Format

      if ((attr.minLength !== undefined && data.length < attr.minLength) ||
          (attr.maxLength !== undefined && data.length > attr.maxLength) ||
          (attr.pattern !== undefined && attr.pattern.exec(data) === null)
        ) {
          res.errors.push({ field: elem, reason: 'FORMAT' });
          continue;
      }

    } else if (attr.type === "int") {

      // Check if NaN

      if (isNaN(data)) {
        res.errors.push({ field: elem, reason: 'NAN' });
        continue;
      }

      data = parseInt(data, 10);

    } else if (attr.type === "object") {
      let model = attr.model;

      if (typeof model === "function") {
        let param = null;

        if (attr.needs !== undefined && options.noRequire !== true &&
          (res.result[attr.needs] === null || res.result[attr.needs] === undefined)) {
          res.result[name] = null;
          continue;
        } else if (attr.needs !== undefined)
          param = res.result[attr.needs];
        model = model(param);
      }

      const tmp = parser(data, model, options);

      tmp.errors.forEach((err) => {
        err.field = `${elem}.${err.field}`;
        res.errors.push(err);
      });
      res.result[name] = tmp.result;
      continue;
    } 

    // Add to results

    res.result[name] = data;
  
    // Modifier

    if (attr.modifier && typeof attr.modifier === 'function')
      res.result[name] = attr.modifier(data);
  }
  return res;
}

export default parser;