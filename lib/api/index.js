import axios from 'axios';
import FormData from 'form-data';
import moment from 'moment';

import constants from '../../constants';
import { mangoPay as mangoPayCred } from '../../credentials';

import { infoLog, errLog, parser, dbLog } from '../../lib';
import { mangoPayLegalModel, mangoPayNaturalModel, mangoPayBankAccountModel } from '../../models';

const apiId = new Buffer(`${mangoPayCred.clientId}:${mangoPayCred.apiKey}`).toString('base64');

const get = async (target, headers) => (await axios.get(`${constants.api}/${target}`, {headers})).data;
const post = async (target, body, headers) => (await axios.post(`${constants.api}/${target}`, body, {headers})).data;
const put = async (target, body, headers) => (await axios.put(`${constants.api}/${target}`, body, {headers})).data;

const getToken = async () => {
  infoLog("FETCHING MANGOPAY TOKEN...");
  const authData = new FormData();
  authData.append('grant_type', 'client_credentials');
  try {
    const res = await post('oauth/token', authData, {
      'Authorization': `Basic ${apiId}`,
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    res.refetchDate = new Date().getTime() + res.expires_in * 1000;
    infoLog("GOT MANGOPAY TOKEN");
    return res;
  } catch (err) {
    errLog("COULD NOT FETCH TOKEN", err);
  }
  return null;
}

let tokenPromise = null;

const init = () => {
  tokenPromise = getToken();
}

const authed = async (data, call) => {
  const tokenObj = await Promise.resolve(tokenPromise);
  if (tokenPromise === null || tokenObj === null || tokenObj.refetchDate < new Date().getTime()) {
    tokenPromise = getToken();
    return authed(data, call);
  }

  return call(data, {
    'Authorization': `Bearer ${tokenObj.access_token}`,
    'Content-Type': 'application/json'
  });
}

const getUser = async (data, header) => {
  try {
    let promises = [];
    promises.push(get(`${mangoPayCred.clientId}/users/${data.pspId}`, header));
    if (data.bankId)
      promises.push(get(`${mangoPayCred.clientId}/users/${data.pspId}/bankaccounts/${data.bankId}`, header));
    else
      promises.push(Promise.resolve(null));

    let apiRes = await Promise.all(promises);

    return {
      user: parser(apiRes[0], data.seller ? mangoPayLegalModel : mangoPayNaturalModel, {reverse: true}).result,
      bank: apiRes[1] ? parser(apiRes[1], mangoPayBankAccountModel, {reverse: true}).result : null,
    };
  } catch (err) {
    errLog("COULD NOT GET USER", err.response.data);
  }
  return null;
}

const postClient = async (data, header) => {
  console.log("POST CLIENT before", data);
  const mangoBody = parser(data, mangoPayNaturalModel).result
  console.log("POST CLIENT mango", mangoBody);
  try {
    return await post(`${mangoPayCred.clientId}/users/natural`, mangoBody, header);
  } catch (err) {
    errLog("COULD NOT POST CLIENT", err.response.data);
  }
  return null;
};

const postBusiness = async (data, header) => {
  console.log("POST BUSINESS before", data);
  const mangoBody = parser(data, mangoPayLegalModel).result
  console.log("POST BUSINESS mango", mangoBody);
  mangoBody.LegalPersonType = "BUSINESS";
  try {
    return await post(`${mangoPayCred.clientId}/users/legal`, mangoBody, header);
  } catch (err) {
    errLog("COULD NOT POST BUSINESS", err.response.data);
  }
  return null;
};

const putClient = async (data, header) => {
  console.log("PACH CLIENT before", data);
  const mangoBody = parser(data, mangoPayNaturalModel).result
  console.log("PACH CLIENT mango", mangoBody);
  try {
    return await put(`${mangoPayCred.clientId}/users/natural/${data.pspId}`, mangoBody, header);
  } catch (err) {
    errLog("COULD NOT MODIF CLIENT", err.response.data);
  }
  return null;
};

const putBusiness = async (data, header) => {
  console.log("PACH BUSINESS before", data);
  const mangoBody = parser(data, mangoPayLegalModel).result
  console.log("PACH BUSINESS mango", mangoBody);
  try {
    return await put(`${mangoPayCred.clientId}/users/legal/${data.pspId}`, mangoBody, header);
  } catch (err) {
    errLog("COULD NOT MODIF BUSINESS", err.response.data);
  }
  return null;
};

const createWallet = async (data, header) => {
  try {
    return await post(`${mangoPayCred.clientId}/wallets`, {
      Owners: [data.pspId],
      Description: `MIP user wallet`,
      Currency: constants.currency,
      Tag: data.mipId
    }, header);
  } catch (err) {
    errLog("COULD NOT CREATE WALLET", err.response.data);
  }
  return null;
}

const setCreditCardType = async (data, header) => {
  console.log("SET CREDIT CARD", data);
  try {
    if (data.replaceId) {
      const replaceRes = await put(`${mangoPayCred.clientId}/cards/${data.replaceId}/`, {
        Active: false,
      }, header);
      console.log("DELETE CARD RES", replaceRes);
    }
    const createRes = await post(`${mangoPayCred.clientId}/cardregistrations`, {
      UserId: data.pspId,
      Currency: data.currency,
      CardType: data.type,
    }, header);
    console.log("SET CREDIT CARD RES", createRes);
    const { AccessKey, PreregistrationData, CardType, Id } = createRes;
    return { id: Id, accessKey: AccessKey, preregistrationData: PreregistrationData, cardType: CardType };
  } catch (err) {
    errLog("COULD NOT SET CREDIT CARD TYPE", err.response.data);
  }
  return null;
}

const setCreditCardKey = async (data, header) => {
  console.log("SET CREDIT CARD KEY", data);
  try {
    if (data.replaceId !== undefined) {
      const replaceRes = await put(`${mangoPayCred.clientId}/cards/${data.replaceId}/`, {
        Active: false,
      }, header);
      console.log("DELETE CARD RES", replaceRes);
    }
    const setKeyRes = await put(`${mangoPayCred.clientId}/CardRegistrations/${data.cardId}`, {
      RegistrationData: data.key,
    }, header);
    console.log("CREDIT CARD KEY RES", setKeyRes);
    return setKeyRes;
  } catch (err) {
    errLog("COULD NOT SET CREDIT CARD KEY", err.response.data);
  }
  return null;
}

const setBankAccount = async (data, header) => {
  const mangoBody = parser(data.bankAccount, mangoPayBankAccountModel).result
  try {
    return await post(`${mangoPayCred.clientId}/users/${data.pspId}/bankaccounts/iban/`, mangoBody, header);
  } catch (err) {
    errLog("COULD NOT SET BANK ACCOUNT", err.response.data);
  }
  return null;
}

const replaceBankAccount = async (data, header) => {
  const mangoBody = parser(data.bankAccount, mangoPayBankAccountModel).result
  try {
    const rmRes = await put(`${mangoPayCred.clientId}/users/${data.pspId}/bankaccounts/${data.bankAccountId}`, {
      "Active": false
    }, header);
    const res = await post(`${mangoPayCred.clientId}/users/${data.pspId}/bankaccounts/iban/`, mangoBody, header);
    return res;
  } catch (err) {
    errLog("COULD NOT SET BANK ACCOUNT", err.response.data);
  }
  return null;
}

const createKyc = async (data, header) => {
  const mangoBody = parser(data);
  /* TODO */
  return null;
}

const payIn = async (data, header) => {
  const { mipId, pspId, amount, walletId, cardId, cardCurrency, cardType, fee } = data;
  console.log("PAYIN DATA", data);
  try {
    const payInRes = await post(`${mangoPayCred.clientId}/payins/card/direct`, {
      Tag: `${mipId}`,
      AuthorId: pspId,
      CardId: cardId,
      DebitedFunds: {
        Currency: cardCurrency,
        Amount: amount + fee,
      },
      Fees: {
        Currency: cardCurrency,
        Amount: fee,
      },
      CardType: cardType,
      CreditedWalletId: walletId,
      SecureModeReturnURL: "http://www.my-site.com/returnURL/",
      Culture: "EN",
      SecureModeNeeded: false,
      StatementDescriptor: "MIP Payin"
    }, header);
    console.log("PAY IN RES", payInRes);
    return {
      userId: payInRes.AuthorId,
      pspCode: payInRes.ResultCode,
      pspMessage: payInRes.ResultMessage,
      success: payInRes.Status === 'FAILED' ? false : true,
      transaction: payInRes.Status !== 'FAILED' ? {
        medium: cardType,
        currency: constants.currency,
        debited: payInRes.DebitedFunds.Amount,
        credited: payInRes.CreditedFunds.Amount,
        fees: payInRes.Fees.Amount,
        date: moment.unix(payInRes.CreationDate).format("YYYY-MM-DD"),
        toDate: moment.unix(payInRes.ExecutionDate).format("YYYY-MM-DD"),
      } : null,
    };
  } catch (err) {
    errLog("COULD NOT MAKE PAYIN", err.response ? err.response.data : err);
    if (err.response.data) {
      return {
        userId: mipId,
        pspCode: null,
        pspMessage: err.response.data.errors,
        success: false,
        transaction: null,
      }
    }
  }
  return null;
}

const payOut = async (data, header) => {
  const { mipId, pspId, amount, walletId, bankId, fee, currency, message } = data;
  try {
    const body = {
      AuthorId: pspId,
      DebitedFunds: {
        Currency: currency,
        Amount: amount + fee,
      },
      Fees: {
        Currency: currency,
        Amount: fee,
      },
      BankAccountId: bankId,
      DebitedWalletId: walletId,
      BankWireRef: message,
    };
    console.log(body);
    const payOutRes = await post(`${mangoPayCred.clientId}/payouts/bankwire`, body, header);
    errLog("COULD NOT MAKE PAYOUT", payOutRes);
    return {
      userId: payOutRes.AuthorId,
      pspCode: payOutRes.ResultCode,
      pspMessage: payOutRes.ResultMessage,
      success: payOutRes.Status === 'FAILED' ? false : true,
      transaction: payOutRes.Status !== 'FAILED' ? {
        medium: "BANK_WIRE",
        currency: currency,
        debited: payOutRes.DebitedFunds.Amount,
        credited: payOutRes.CreditedFunds.Amount,
        fees: payOutRes.Fees.Amount,
        date: moment.unix(payOutRes.CreationDate).format("YYYY-MM-DD"),
        toDate: moment.unix(payOutRes.ExecutionDate).format("YYYY-MM-DD"),
      } : null,
    };
  } catch (err) {
    errLog("COULD NOT MAKE PAYOUT", err.response ? err.response.data : err);
    if (err.response.data) {
      return {
        userId: mipId,
        pspCode: null,
        pspMessage: err.response.data.errors,
        success: false,
        transaction: null,
      }
    }
  }
  return null;
}

const Api = {
  init,
  getUser: (data) => authed(data, getUser),
  createNaturalUser: (data) => authed(data, postClient),
  createLegalUser: (data) => authed(data, postBusiness),
  createWallet: (data) => authed(data, createWallet),
  updateNaturalUser:(data) => authed(data, putClient),
  updateLegalUser: (data) => authed(data, putBusiness),
  setCreditCardType: (data) => authed(data, setCreditCardType),
  setCreditCardKey: (data) => authed(data, setCreditCardKey),
  setBankAccount: (data) => authed(data, setBankAccount),
  replaceBankAccount: (data) => authed(data, replaceBankAccount),
  createKyc: (data) => authed(data, createKyc),
  payOut: (data) => authed(data, payOut),
  payIn: (data) => authed(data, payIn),
};


export default Api;