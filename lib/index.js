export const errLog = (message, error) => {
  console.error(`\x1b[31m/!\\  ${message}  /!\\\x1b[0m`);
  if (error)
    console.error(error);
}

export const infoLog = (message, data) => {
  console.log(`===  ${message}  ===`);
  if (data)
    console.log(data);
}

export const dbLog = (message, data) => {
  console.log(`\x1b[33m=>  ${message}\x1b[0m`);
  if (data)
    console.log(data);
}

export const pad = (str, pad) => {
  const l = str.length;
  const left = Math.floor((pad - l) / 2);
  const right = Math.floor((pad - l) / 2) + (l % 2 ? 1 : 0);
  return Array(left + 1).join(' ') + str + Array(right + 1).join(' ');
}

export { default as parser } from './parser';
export { default as sql } from './sql';
export { default as Api } from './api';
export * from './utils'
export * from './middlewares';