import pg from 'pg';
import constants from '../../constants';
import { sql as sqlParams } from '../../credentials';
import { errLog } from '..';

const { Pool } = pg;
console.log("HERE", process.env.HEROKU_POSTGRESQL_BRONZE_URL);
const client = new Pool({
  connectionString: process.env.HEROKU_POSTGRESQL_BRONZE_URL,
  ssl: { rejectUnauthorized: false }
});

client.connect();

const dbLoader = (f, params) => {
  // const connection = mysql.createConnection({
  //     host: sqlParams.host,
  //     port: 5432,
  //   user: sqlParams.user,
  //   password: sqlParams.password,
  //   database: constants.database
  // });
  const res = f(client, params);
  //client.end();
  return res;
};


const getUser = (sql, id) => {
  return new Promise((resolve, reject) => {
    const tmp = 
    sql.query('SELECT * FROM users WHERE mipId = $1::text', [id], (err, res) => {
      console.log("SQL GET", err, res);
      if (err)
        reject(err);
      else {
        if (res.rows.length === 0)
          resolve(null);
        else
          resolve(res.rows[0]);
      }
    });
  });
};

const addUser = async (sql, {
  mipId, 
  pspId, 
  seller, 
  walletId, 
  cardId, 
  cardType, 
  cardCurrency, 
  bankId
}) => {
  return new Promise((resolve, reject) => {
    try {
      const status = walletId && cardId && bankId ? "FILLED" : "CREATED";
      sql.query(
      'INSERT INTO users(mipId, pspId, seller, status, walletId, cardType, cardCurrency, cardId, bankId) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)', 
      [mipId, pspId, seller, status, walletId, cardType, cardCurrency, cardId, bankId], (err, res) => {
        if (err) {
          errLog("SQL ADD USER", err);
          resolve(null);
        } else
          resolve(res.rows[0]);
      });
    } catch (err) {
      errLog("SQL ADD USER2", err);
      resolve(null);
    }
  });
};

const updateUserItems = async (sql, {
  mipId,
  cardId,
  cardType,
  cardCurrency,
  cardKey,
  bankId
}) => {
  const status = cardId && bankId ? "FILLED" : "CREATED";
  return new Promise((resolve, reject) => {
    try {
      let query = 'UPDATE users SET status=$1';
      let params = [status];
      let counter = 2;

      if (bankId) {
        query += ', bankId=$' + counter++;
        params.push(bankId);
      }
      if (cardId) {
        query += ', cardId=$' + counter++;
        params.push(cardId);
      }
      if (cardType && cardCurrency) {
        query += ', cardType=$' + counter + ', cardCurrency=$' + ++counter;
        params.push(cardType);
        params.push(cardCurrency);
        cardKey = null;
      }
      if (cardKey && cardType === undefined) {
        query += ', cardKey=$' + counter++;
        params.push(cardKey);
      }
      query += ' WHERE mipId = $' + counter;
      params.push(mipId);

      console.log("SQL SET", query, params);

      sql.query(query, params, (err, res) => {
        if (err) {
          errLog("UPDATE USER ERR", err);
          resolve(null);
        } else
          resolve(res.rows[0]);
      });
    } catch (err) {
      errLog("UPDATE USER ERR2", err);
      resolve(null);
    }
  });
};

const functions = {
  getUser: (args) => dbLoader(getUser, args),
  addUser: (args) => dbLoader(addUser, args),
  updateUserItems: (args) => dbLoader(updateUserItems, args)
};

export default functions;
