-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 12, 2020 at 06:29 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mip_pay_prod`
--

CREATE DATABASE mip_pay_prod;
USE mip_pay_prod;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `mipId` varchar(50) NOT NULL,
  `pspId` varchar(50) NOT NULL,
  `seller` tinyint(1) NOT NULL DEFAULT '0',
  `status` varchar(10) NOT NULL DEFAULT 'CREATED',
  `walletId` varchar(50) NOT NULL,
  `cardId` varchar(50) DEFAULT NULL,
  `cardType` varchar(50) DEFAULT NULL,
  `cardCurrency` varchar(3) DEFAULT NULL,
  `cardKey` varchar(50) DEFAULT NULL,
  `bankId` varchar(50) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`mipId`),
  ADD UNIQUE KEY `mipId` (`mipId`),
  ADD UNIQUE KEY `mangoId` (`pspId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

--
-- Database: `mip_pay_prod`
--

CREATE DATABASE mip_pay_test;

USE mip_pay_test;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `mipId` varchar(50) NOT NULL,
  `pspId` varchar(50) NOT NULL,
  `seller` tinyint(1) NOT NULL DEFAULT '0',
  `status` varchar(10) NOT NULL DEFAULT 'CREATED',
  `walletId` varchar(50) NOT NULL,
  `cardId` varchar(50) DEFAULT NULL,
  `cardType` varchar(50) DEFAULT NULL,
  `cardCurrency` varchar(3) DEFAULT NULL,
  `cardKey` varchar(50) DEFAULT NULL,
  `bankId` varchar(50) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`mipId`),
  ADD UNIQUE KEY `mipId` (`mipId`),
  ADD UNIQUE KEY `mangoId` (`pspId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
