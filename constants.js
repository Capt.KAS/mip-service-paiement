const constants = {};

if (process.env["ENV"] === "PROD") {
  constants.hostname = '51.210.180.45';
  constants.port = 8080;
  constants.apiHost = "https://api.sandbox.mangopay.com";
  constants.apiVersion = "V2.01";
  constants.database = "d58pgqqi8j7tqf";
} else if (process.env["ENV"] === "TEST") {
  constants.hostname = '51.210.180.45';
  constants.port = 8081;
  constants.apiHost = "https://api.sandbox.mangopay.com";
  constants.apiVersion = "V2.01";
  constants.database = "mip_pay_test";
} else if (process.env["ENV"] === "LOCAL") {
  constants.hostname = '127.0.0.1';
  constants.port = 8080;
  constants.apiHost = "https://api.sandbox.mangopay.com";
  constants.apiVersion = "V2.01";
  constants.database = "mip_pay";
}

constants.currency = "EUR"; //XAF
constants.api = `${constants.apiHost}/${constants.apiVersion}`

export default constants;
